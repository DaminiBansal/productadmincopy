from django.db import models

# Create your models here.

# class Login_Master(models.Model):
# 	username  = models.CharField(primary_key=True,max_length=100),
# 	password  = models.CharField(max_length=100)

# class level(models.Model):
#     username  = models.CharField(primary_key=True,max_length=100),
#     password  = models.CharField(max_length=100)

class Product_Master(models.Model):
	PRODUCT_CODE         = models.CharField(primary_key=True,max_length=200)
	PRODUCT_DESCRIPTION  = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL1_CODE     = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL1_DESC     = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL2_CODE     = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL2_DESC     = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL3_CODE     = models.CharField(max_length=200,blank=True, null=True)
	PROD_LEVEL3_DESC     = models.CharField(max_length=200,blank=True, null=True)
	BRAND_LEVEL1_CODE    = models.CharField(max_length=200,blank=True, null=True)
	BRAND_LEVEL1_DESC    = models.CharField(max_length=200,blank=True, null=True)
	BRAND_LEVEL2_CODE    = models.CharField(max_length=200,blank=True, null=True)
	BRAND_LEVEL2_DESC    = models.CharField(max_length=200,blank=True, null=True)
	ACTIVE_FLAG          = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY1     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY2     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY3     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY4     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY5     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY6     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY7     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY8     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY9     = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY10    = models.CharField(max_length=200,blank=True, null=True)
	CUSTOM_CATEGORY11    = models.CharField(max_length=200,blank=True, null=True)


