from django.contrib import admin
from .models import Product_Master

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    model = Product_Master
    #resource_class = DataResource
    #
    search_fields = ('PRODUCT_CODE','PROD_LEVEL2_DESC')
    list_display = ('PRODUCT_CODE','PROD_LEVEL2_DESC', )
admin.site.register(Product_Master,ProductAdmin)