from django.shortcuts import render
from django.contrib.auth import  login as auth_login , authenticate
from .tables import ProductTable
from .models import Product_Master

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control

from django.http import HttpResponse, HttpResponseNotFound
import xlwt

# Create your views here.
def login(request):
    categorylist = []
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = authenticate(username = username, password = password)
    if user is not None:
        auth_login(request, user)
        products = ProductTable(Product_Master.objects.filter(PROD_LEVEL2_DESC=None))
        return render(request, "productsList.html", {"categorylist":getCategoryList(),"productdetails":products})
    else:
        return render(request, 'registration/login.html')

def refrigeratorDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='Refrigerators')
    return render(request, 'src/refrigerator-datatable.html',{'products':products})

def refrigeratorEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/refrigerator-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateRefrigeratorDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '') 
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '') 
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '') 
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='Refrigerators')
    return render(request, 'src/refrigerator-datatable.html',{'products':products})

def televisionDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    return render(request, 'src/television-datatable.html',{'products':products})

def televisionEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/television-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateTelevisionDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '') 
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '') 
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '') 
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    return render(request, 'src/television-datatable.html',{'products':products})

def kitchenappliancesDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='KITCHEN APPLANCES')
    return render(request, 'src/kitchenappliances-datatable.html',{'products':products})

def kitchenappliancesEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/kitchenappliances-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateKitchenappliancesDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='KITCHEN APPLANCES')
    return render(request, 'src/kitchenappliances-datatable.html',{'products':products})

def airconditionerDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AIR CONDITIONERS')
    return render(request, 'src/airconditioner-datatable.html',{'products':products})

def airconditionerEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/airconditioner-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateairconditionerDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AIR CONDITIONERS')
    return render(request, 'src/airconditioner-datatable.html',{'products':products})

def fanDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='FANS')
    return render(request, 'src/fan-datatable.html',{'products':products})

def fanEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/fan-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updatefanDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='FANS')
    return render(request, 'src/fan-datatable.html',{'products':products})

def ironDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='IRONS')
    return render(request, 'src/iron-datatable.html',{'products':products})

def ironEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/iron-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateironDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='IRONS')
    return render(request, 'src/iron-datatable.html',{'products':products})

def ricecookerDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='RICE COOKERS')
    return render(request, 'src/ricecooker-datatable.html',{'products':products})

def ricecookerEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/ricecooker-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updatericecookerDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='RICE COOKERS')
    return render(request, 'src/ricecooker-datatable.html',{'products':products})

def audioDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AUDIOS')
    return render(request, 'src/audio-datatable.html',{'products':products})

def audioEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/audio-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updateaudioDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY7 = request.POST.get('customcategory7', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AUDIOS')
    return render(request, 'src/audio-datatable.html',{'products':products})

def sewingmachineDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='SEWING MACHINES')
    return render(request, 'src/sewingmachine-datatable.html',{'products':products})

def sewingmachineEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/sewingmachine-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updatesewingmachineDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='SEWING MACHINES')
    return render(request, 'src/sewingmachine-datatable.html',{'products':products})

def washingmachineDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WASHING MACHINE')
    return render(request, 'src/washingmachine-datatable.html',{'products':products})

def washingmachineEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/washingmachine-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updatewashingmachineDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')    
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WASHING MACHINE')
    return render(request, 'src/washingmachine-datatable.html',{'products':products})

def waterpumpDetail(request):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WATER PUMPS')
    return render(request, 'src/waterpump-datatable.html',{'products':products})

def waterpumpEditForm(request,productId):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/waterpump-editform.html',{'productDetail':productDetail})


# @cache_control(no_cache=True, must_revalidate=True)
# @login_required(login_url='/accounts/login/')
def updatewaterpumpDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WATER PUMPS')
    return render(request, 'src/waterpump-datatable.html',{'products':products})

def export_refrigerator_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "RefrigeratorsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Refrigerators Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity(in ltrs)', 'Cooling Type', 'No of Doors', 
               'Inverter', 'Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='Refrigerators')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_television_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "TelevisionsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Televisions Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Size(in inchs)', 'Smart', 'Resolution', 
               'Operating System', 'Shape','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_kitchenappliance_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "KitchenAppliancesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Kitchen Appliances Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'SubCategory','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='KITCHEN APPLANCES')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 6, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_airconditioner_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "AirConditionersProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Air Conditioners Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Type', 'Inverter', 'Capacity(BTU wise)', 
               'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AIR CONDITIONERS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 8, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_fan_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "FansProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Fans Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Use', 'Type', 'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='FANS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 7, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_iron_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "IronsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Irons Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Weight', 'Functionality', 'Sole Plate', 
               'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='IRONS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 8, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_ricecooker_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "RiceCookersProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Rice Cookers Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity(in ltrs)', 'Type', 'Multicooker', 
               'Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='RICE COOKERS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 9, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_audio_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "AudiosProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Audios Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Subcategory', 'Tower', 'Bluetooth', 
               'USB','CD/DVD','Wattage(Value)','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AUDIOS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY7)
        ws.write(row_num, 10, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 11, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_sewingmachine_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "SewingMachinesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Sewing Machines Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Type', 'Stitch Type', 'Hi-Tech', 
               'Use','Weight','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='SEWING MACHINES')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_washingmachine_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "WashingMachinesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Washing Machines Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity in Litres', 'Automatic Type', 'Load', 
               'Inverter','Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WASHING MACHINE')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def export_waterpump_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "WaterPumpsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Water Pumps Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Category', 'Pump Type', 'Submersible Type', 
               'Horse Power Rating','Pipes','Factory vs Direct Imports','Country of Origin of Motor','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WATER PUMPS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)        
        ws.write(row_num, 9, product.CUSTOM_CATEGORY7)
        ws.write(row_num, 10, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 11, product.ACTIVE_FLAG)

    wb.save(response)
    return response

def getCategoryList():
    categorylist = set()
    p = Product_Master.objects.all()
    for product in p:
        categorylist.add(product.PROD_LEVEL2_DESC)
    return categorylist

