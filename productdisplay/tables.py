from table import Table
from .models import Product_Master
from table.columns import Column,LinkColumn,Link
from table.utils import A

class ProductTable(Table):
    
    productcode = LinkColumn(header=u'Product Code', links=[Link(text=A('PRODUCT_CODE'), viewname='editProductDetails',args=(A('PRODUCT_CODE'),) ),])
    #productcode   = Column(field='PRODUCT_CODE',header='Product Code',)
    productdesc   = Column(field='PRODUCT_DESCRIPTION',header='Product Desc',)
    branddesc     = Column(field='BRAND_LEVEL2_DESC',header='Brand Desc',)
    activeflag    = Column(field='ACTIVE_FLAG',header='Active Flag',)
    custom_field1 = Column(field='CUSTOM_CATEGORY1',header='Custom Category1',)
    custom_field2 = Column(field='CUSTOM_CATEGORY2',header='Custom Category2',)
    custom_field3 = Column(field='CUSTOM_CATEGORY3',header='Custom Category3',)
    custom_field4 = Column(field='CUSTOM_CATEGORY4',header='Custom Category4',)
    custom_field5 = Column(field='CUSTOM_CATEGORY5',header='Custom Category5',)

    class Meta:
        model = Product_Master