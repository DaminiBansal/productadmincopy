## v2.1.5
- update: `@coreui/coreui-pro` to `2.0.12` fixes some IE11 and `rtl` issues
- update: `@babel/cli` to `7.1.2`
- update: `@babel/core` to `7.1.2`
- update: `eslint to `5.6.1`
- update: `foreach-cli` to `1.8.1`
 
## v2.1.4
- feat(switches): add missing `disabled` switch example
- update: `@coreui/coreui-pro` to `2.0.11` fixes some IE11 and `rtl` issues
- chore: update `babel-eslint` to `10.0.1`
- chore: update `stylelint` to `9.6.0`

## v2.1.3
- feat(basic-forms): add missing `date-input` 
- update: `@coreui/coreui-pro` to `2.0.9`
- update: `codemirror` to `5.40.2`
- update: `flag-icon-css` to `3.2.0`
- update: `jquery-validation`to `1.18.0`
- update: `@babel/cli` to `7.1.0`
- update: `@babel/core` to `7.1.0`
- update: `@babel/preset-env` to `7.1.0`
- update: `eslint` to `5.6.0`
- update: `js-beautify` to `1.8.6`
- update: `stylelint-scss` to `3.3.1`

## v2.1.2
- Update: `@coreui/coreui-pro` to `^2.0.7`
- Update: `bootstrap` to `^4.1.3`
- Update: `codemirror` to `^5.40.0`
- Update: `@babel/cli` to `^7.0.0`
- Update: `@babel/core` to `^7.0.0`
- Update: `@babel/plugin-proposal-object-rest-spread` to `^7.0.0`
- Update: `@babel/preset-env` to `^7.0.0`
- Update: `autoprefixer` to `^9.1.5`
- Update: `babel-eslint` to `^9.0.0`
- Update: `browser-sync` to `^2.24.7`
- Update: `copyfiles` to `^2.1.0`
- Update: `eslint` to `^5.5.0`
- Update: `js-beautify` to `^1.8.4`
- Update: `nodemon` to `^1.18.4`
- Update: `stylelint` to `^9.5.0`

## v2.1.1
- **Feat: Add pug support**
- Update: @coreui/coreui-icons to 0.3.0
- Update: @coreui/coreui to 2.0.3
- Update: perfect-scrollbar to 1.4.0

## v2.1.0
- Update: @babel/cli to 7.0.0-rc.1
- Update: @babel/core to 7.0.0-rc.1
- Update: @babel/plugin-proposal-object-rest-spread to 7.0.0-rc.1
- Update: @babel/preset-env to 7.0.0-rc.1
- Update: autoprefixer to 9.1.1
- Update: babel-eslint to 8.2.6
- Update: bootstrap to 4.1.3
- Update: bootstrap-daterangepicker to 3.0.3
- Update: browser-sync to 2.24.6
- Update: clean-css-cli to 4.2.1
- Update: codemirror to 5.39.2
- Update: cross-env to 5.2.0
- Update: datatables.net-bs4 to 1.10.19
- Update: eslint to 5.3.0
- Update: eslint-plugin-compat to 2.5.1
- Update: node-sass to 4.9.3
- Update: nodemon to 1.18.3
- Update: popper.js to 1.14.4
- Update: postcss-cli to 6.0.0
- Update: shelljs to 0.8.2
- Update: stylelint to 9.4.0
- Update: stylelint-order to 1.0.0
- Update: stylelint-scss to 3.3.0
