Django==2.1.3
mysqlclient==1.3.12
numpy==1.15.3
pytz==2018.7
table==0.0.4
xlwt==1.3.0
django-datatable==0.3.1