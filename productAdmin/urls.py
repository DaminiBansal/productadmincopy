"""productAdmin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url 
from django.urls import path

from django.contrib.auth import views 
from productdisplay.views import (login,updateRefrigeratorDetails,refrigeratorDetail,refrigeratorEditForm,
    televisionEditForm,televisionDetail,updateTelevisionDetails,
    kitchenappliancesEditForm,kitchenappliancesDetail,updateKitchenappliancesDetails,
    airconditionerEditForm,airconditionerDetail,updateairconditionerDetails,
    fanEditForm,fanDetail,updatefanDetails,
    ironEditForm,ironDetail,updateironDetails,
    washingmachineEditForm,washingmachineDetail,updatewashingmachineDetails,
    ricecookerEditForm,ricecookerDetail,updatericecookerDetails,
    audioEditForm,audioDetail,updateaudioDetails,
    sewingmachineEditForm,sewingmachineDetail,updatesewingmachineDetails,
    waterpumpEditForm,waterpumpDetail,updatewaterpumpDetails,
    export_refrigerator_productdetails_xls,export_kitchenappliance_productdetails_xls,export_television_productdetails_xls,
    export_airconditioner_productdetails_xls,export_fan_productdetails_xls,export_iron_productdetails_xls,
    export_washingmachine_productdetails_xls,export_ricecooker_productdetails_xls,export_audio_productdetails_xls,
    export_sewingmachine_productdetails_xls,export_waterpump_productdetails_xls)
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^$', TemplateView.as_view(template_name='src/index.html')),
    url(r'^product-details/refrigerators/editform/(.*)/$', refrigeratorEditForm,name ='refrigeratorEditForm'),
    url(r'^product-details/refrigerators/$', refrigeratorDetail,name ='refrigeratorDetail'),
    url(r'^product-details/refrigerators/update-details/(.*)/$',updateRefrigeratorDetails, name='updateRefrigeratorDetails'),
    url(r'^product-details/television/editform/(.*)/$', televisionEditForm,name ='televisionEditForm'),
    url(r'^product-details/television/$', televisionDetail,name ='televisionDetail'),
    url(r'^product-details/television/update-details/(.*)/$',updateTelevisionDetails, name='updateTelevisionDetails'),
    url(r'^product-details/kitchenappliances/editform/(.*)/$', kitchenappliancesEditForm,name ='kitchenappliancesEditForm'),
    url(r'^product-details/kitchenappliances/$', kitchenappliancesDetail,name ='kitchenappliancesDetail'),
    url(r'^product-details/kitchenappliances/update-details/(.*)/$',updateKitchenappliancesDetails, name='updateKitchenappliancesDetails'),
    url(r'^product-details/airconditioner/editform/(.*)/$', airconditionerEditForm,name ='airconditionerEditForm'),
    url(r'^product-details/airconditioner/$', airconditionerDetail,name ='airconditionerDetail'),
    url(r'^product-details/airconditioner/update-details/(.*)/$',updateairconditionerDetails, name='updateairconditionerDetails'),
    url(r'^product-details/fan/editform/(.*)/$', fanEditForm,name ='fanEditForm'),
    url(r'^product-details/fan/$', fanDetail,name ='fanDetail'),
    url(r'^product-details/fan/update-details/(.*)/$',updatefanDetails, name='updatefanDetails'),
    url(r'^product-details/iron/editform/(.*)/$', ironEditForm,name ='ironEditForm'),
    url(r'^product-details/iron/$', ironDetail,name ='ironDetail'),
    url(r'^product-details/iron/update-details/(.*)/$',updateironDetails, name='updateironDetails'),
    url(r'^product-details/washingmachine/editform/(.*)/$', washingmachineEditForm,name ='washingmachineEditForm'),
    url(r'^product-details/washingmachine/$', washingmachineDetail,name ='washingmachineDetail'),
    url(r'^product-details/washingmachine/update-details/(.*)/$',updatewashingmachineDetails, name='updatewashingmachineDetails'),
    url(r'^product-details/ricecooker/editform/(.*)/$', ricecookerEditForm,name ='ricecookerEditForm'),
    url(r'^product-details/ricecooker/$', ricecookerDetail,name ='ricecookerDetail'),
    url(r'^product-details/ricecooker/update-details/(.*)/$',updatericecookerDetails, name='updatericecookerDetails'),
    url(r'^product-details/audio/editform/(.*)/$', audioEditForm,name ='audioEditForm'),
    url(r'^product-details/audio/$', audioDetail,name ='audioDetail'),
    url(r'^product-details/audio/update-details/(.*)/$',updateaudioDetails, name='updateaudioDetails'),
    url(r'^product-details/sewingmachine/editform/(.*)/$', sewingmachineEditForm,name ='sewingmachineEditForm'),
    url(r'^product-details/sewingmachine/$', sewingmachineDetail,name ='sewingmachineDetail'),
    url(r'^product-details/sewingmachine/update-details/(.*)/$',updatesewingmachineDetails, name='updatesewingmachineDetails'),
    url(r'^product-details/waterpump/editform/(.*)/$', waterpumpEditForm,name ='waterpumpEditForm'),
    url(r'^product-details/waterpump/$', waterpumpDetail,name ='waterpumpDetail'),
    url(r'^product-details/waterpump/update-details/(.*)/$',updatewaterpumpDetails, name='updatewaterpumpDetails'),

    url(r'^product-details/refrigerators/export$', export_refrigerator_productdetails_xls,name ='export_refrigerator_productdetails_xls'),
    url(r'^product-details/television/export$', export_television_productdetails_xls,name ='export_television_productdetails_xls'),
    url(r'^product-details/kitchenappliances/export$', export_kitchenappliance_productdetails_xls,name ='export_kitchenappliance_productdetails_xls'),
    url(r'^product-details/airconditioner/export$', export_airconditioner_productdetails_xls,name ='export_airconditioner_productdetails_xls'),
    url(r'^product-details/fan/export$', export_fan_productdetails_xls,name ='export_fan_productdetails_xls'),
    url(r'^product-details/iron/export$', export_iron_productdetails_xls,name ='export_iron_productdetails_xls'),
    url(r'^product-details/washingmachine/export$', export_washingmachine_productdetails_xls,name ='export_washingmachine_productdetails_xls'),
    url(r'^product-details/ricecooker/export$', export_ricecooker_productdetails_xls,name ='export_ricecooker_productdetails_xls'),
    url(r'^product-details/audio/export$', export_audio_productdetails_xls,name ='export_refrigerator_productdetails_xls'),
    url(r'^product-details/sewingmachine/export$', export_sewingmachine_productdetails_xls,name ='export_sewingmachine_productdetails_xls'),
    url(r'^product-details/waterpump/export$', export_waterpump_productdetails_xls,name ='export_waterpump_productdetails_xls'),
    url(r'^login/$', login, name='login')
    
]
